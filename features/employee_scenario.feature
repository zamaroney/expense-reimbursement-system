Feature: Employee scenario

  Scenario: A employee is on the home page and would like to log in with correct credentials
    Given a employee is on the login page
    When the employee enters the correct username and the correct password for an employee
      And the employee pushes login button
    Then the employee is redirect to the menu

  Scenario: a employee is in the menu and wants to submit a reimbursement
    Given a employee is on the menu page
    When a employee enters valid money and reason
      And a employee presses the submit button
    Then the submission is sent
    When a employee clicks on reimbursement tab
    Then show the reimbursement table
