from behave import *
import time


@given('a employee is on the login page')
def test_on_login_page(context):
    context.driver.get('http://127.0.0.1:5000/')


@when('the employee enters the correct username and the correct password for an employee')
def test_user_input(context):
    context.login.enter_credentials_employee()


@when('the employee pushes login button')
def test_login_button(context):
    context.login.click_login_button()


@then('the employee is redirect to the menu')
def test_user_redirect(context):
    time.sleep(2)
    assert 'employee' in context.driver.current_url


@given('a employee is on the menu page')
def test_on_menu(context):
    assert 'employee' in context.driver.current_url


@when('a employee enters valid money and reason')
def test_valid_reimbursement(context):
    context.employee.enter_reimbursement_info()


@when('a employee presses the submit button')
def test_submit_button(context):
    context.employee.click_submit_button()


@then('the submission is sent')
def test_user_redirect(context):
    time.sleep(2)
    assert 'employee' in context.driver.current_url


@when('a employee clicks on reimbursement tab')
def test_tab_button(context):
    context.employee.click_on_tab()


@then('show the reimbursement table')
def test_show_table(context):
    time.sleep(3)
    assert context.driver.find_element_by_id('reimbursementListBody')
