from behave import *
import time


@given('a manager is on the login page')
def test_on_login_page(context):
    context.driver.get('http://127.0.0.1:5000/')


@when('the manager enters the correct username and the correct password for a manager')
def test_user_input(context):
    context.login.enter_credentials_manager()


@when('the manager pushes login button')
def test_login_button(context):
    context.login.click_login_button()


@then('the manager is redirect to the menu')
def test_user_redirect(context):
    time.sleep(2)
    assert 'manager' in context.driver.current_url


@given('a manager is on the menu page')
def test_manager_ison_menu(context):
    assert 'manager' in context.driver.current_url


@when('a manager enters a valid reimbursement ID, reason and action')
def test_manager_input(context):
    context.manager.enter_reimbursement_change()


@when('a manager presses the submit button')
def test_press_submit(context):
    context.manager.click_submit_button()


@then('the reimbursement is set to approved')
def test_user_redirect(context):
    time.sleep(2)
    assert 'manager' in context.driver.current_url


@when('the manager clicks on the approved tab')
def test_approved_tab(context):
    context.manager.click_approved_tab()


@then('the approved table of reimbursement is shown')
def test_view_approved_table(context):
    time.sleep(2)
    assert context.driver.find_element_by_id('approvedReimbursementBody')


@when('the manager clicks on the rejected tab')
def test_rejected_tab(context):
    context.manager.click_rejected_tab()


@then('the rejected reimbursements are shown')
def test_view_rejected_table(context):
    time.sleep(2)
    assert context.driver.find_element_by_id('rejectedReimbursementBody')


@when('the manager clicks on the statistics tab')
def test_statistic_tab(context):
    context.manager.click_statistic_tab()


@then('the statistics page is shown')
def test_view_statistics_table(context):
    time.sleep(2)
    assert context.driver.find_element_by_id('most-money-spent-title')
