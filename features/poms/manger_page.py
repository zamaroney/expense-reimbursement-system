from selenium.webdriver.support.ui import Select
from src.sql.dbconfig import get_connection


class manager_page:
    reimbursement_id = 'id_submit'
    reason_id = 'reason_submit'
    action_id = 'action-select'
    submit_id = 'submitID'
    approved_tab_id = 'approved-reimbursement-tab'
    rejected_tab_id = 'rejected-reimbursement-tab'
    statistics_tab_id = 'statistics-tab'

    def __init__(self, driver):
        self.driver = driver

    def enter_reimbursement_change(self):
        try:
            conn = get_connection()
            cur = conn.cursor()
            cur.execute(
                "SELECT reimbursement_id FROM reimbursements WHERE reimbursement_id=(SELECT max(reimbursement_id)"
                "FROM reimbursements)")
            test_id = cur.fetchone()
        finally:
            conn.close()
        self.driver.find_element_by_id(self.reimbursement_id).send_keys(str(test_id))
        self.driver.find_element_by_id(self.reason_id).send_keys('Auto Test Reason')
        select = Select(self.driver.find_element_by_id(self.action_id))
        select.select_by_visible_text('Approve')

    def click_submit_button(self):
        self.driver.find_element_by_id(self.submit_id).click()

    def click_approved_tab(self):
        self.driver.find_element_by_id(self.approved_tab_id).click()

    def click_rejected_tab(self):
        self.driver.find_element_by_id(self.rejected_tab_id).click()

    def click_statistic_tab(self):
        self.driver.find_element_by_id(self.statistics_tab_id).click()
