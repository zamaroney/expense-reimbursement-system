class employee_page:
    money_input_id = 'moneyID'
    reason_input_id = 'reasonID'
    submit_button_id = 'submitID'
    tab_id = 'ReimbursementTab'

    def __init__(self, driver):
        self.driver = driver

    def enter_reimbursement_info(self):
        self.driver.find_element_by_id(self.money_input_id).send_keys('1337')
        self.driver.find_element_by_id(self.reason_input_id).send_keys('Auto Test')

    def click_submit_button(self):
        self.driver.find_element_by_id(self.submit_button_id).click()

    def click_on_tab(self):
        self.driver.find_element_by_id(self.tab_id).click()
