from selenium.webdriver.support.ui import Select

class login_page:
    login_button_id = 'signInButton'
    user_selector_id = 'signInButton'
    select_id = 'selectLogin'
    password_field = 'passwordInput'
    username_field = 'usernameField'

    def __init__(self, driver):
        self.driver = driver

    def enter_credentials_employee(self):
        self.driver.find_element_by_id(self.username_field).send_keys('draven')
        self.driver.find_element_by_id(self.password_field).send_keys('draven')
        select = Select(self.driver.find_element_by_id(self.select_id))
        select.select_by_visible_text('Employee')

    def enter_credentials_manager(self):
        self.driver.find_element_by_id(self.username_field).send_keys('headchooper')
        self.driver.find_element_by_id(self.password_field).send_keys('Noxus123')
        select = Select(self.driver.find_element_by_id(self.select_id))
        select.select_by_visible_text('Manager')

    def click_login_button(self):
        self.driver.find_element_by_id(self.login_button_id).click()
