from selenium import webdriver
from features.poms.login_page import login_page
from features.poms.employee_page import employee_page
from features.poms.manger_page import manager_page
from src.sql.dbconfig import get_connection


def before_all(context):
    context.driver = webdriver.Chrome()
    context.login = login_page(context.driver)
    context.employee = employee_page(context.driver)
    context.manager = manager_page(context.driver)


def before_step(context, step):
    pass


def before_scenario(context, scenario):
    pass


def before_feature(context, feature):
    pass


def before_tag(context, tag):
    pass


def after_tag(context, tag):
    pass


def after_feature(context, feature):
    pass


def after_scenario(context, scenario):
    pass


def after_step(context, step):
    pass


def after_all(context):
    context.driver.quit()
    try:
        conn = get_connection()
        cur = conn.cursor()

        cur.execute("DELETE FROM reimbursements WHERE reimbursement_id=(SELECT max(reimbursement_id) FROM "
                    "reimbursements)")

        conn.commit()
    finally:
        conn.close()
