Feature: Manager Scenaio

  Scenario: A manager is on the home page and would like to log in with correct credentials
    Given a manager is on the login page
    When the manager enters the correct username and the correct password for a manager
      And the manager pushes login button
    Then the manager is redirect to the menu

  Scenario: A manager is in the menu and wants to approve a reimbursement and view current reimbursements
    Given a manager is on the menu page
    When a manager enters a valid reimbursement ID, reason and action
      And a manager presses the submit button
    Then the reimbursement is set to approved
    When the manager clicks on the approved tab
    Then the approved table of reimbursement is shown
    When the manager clicks on the rejected tab
    Then the rejected reimbursements are shown
    When the manager clicks on the statistics tab
    Then the statistics page is shown




