from flask import Flask
from flask_session import Session
from flask_cors import CORS

app = Flask(__name__, template_folder="./templates", static_folder="./static")

app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
app.config['SECRET_KEY'] = 'secret!'

Session(app)
CORS(app)
