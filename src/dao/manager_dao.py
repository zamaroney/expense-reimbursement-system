from src.sql.dbconfig import get_connection


def return_manager_info(username, password):
    try:
        conn = get_connection()
        cur = conn.cursor()
        cur.execute("SELECT * FROM managers WHERE manager_username=%s AND manager_password=%s", [username, password])
        employee = cur.fetchone()

        return employee
    finally:
        conn.close()
