from src.sql.dbconfig import get_connection
from src.logger import dao_log


def get_all_reimbursements():
    try:
        conn = get_connection()
        cur = conn.cursor()

        cur.execute("SELECT * FROM reimbursements")

        query_rows = cur.fetchall()
    finally:
        dao_log.info('return all the reimbursements')
        conn.close()

    return query_rows


def get_all_reimbursements_employee(employee_id):
    try:
        conn = get_connection()
        cur = conn.cursor()

        cur.execute("SELECT * FROM reimbursements WHERE author_id=%s", [employee_id])

        query_rows = cur.fetchall()
    finally:
        dao_log.info(f'Return all reimbursements for employee {employee_id}')
        conn.close()

    return query_rows


def change_reimbursement_status(reimbursement_id, status, reason, manager_id):
    try:
        conn = get_connection()
        cur = conn.cursor()

        cur.execute("UPDATE reimbursements SET status=%s, status_reason=%s, manager_id=%s where reimbursement_id=%s"
                    "RETURNING *", [status, reason, manager_id, reimbursement_id])
        reimbursement = cur.fetchone()
        conn.commit()
    finally:
        dao_log.info(f'Changed reimbursement {reimbursement_id} to {status}')
        conn.close()

    return reimbursement


def submit_reimbursements(employee_id, reason, money):
    try:
        conn = get_connection()
        cur = conn.cursor()

        cur.execute("Insert Into reimbursements(author_id, riot_points, reason) values(%s, %s, %s) RETURNING *",
                    [employee_id, money, reason])
        reimbursement = cur.fetchone()
        conn.commit()

    finally:
        dao_log.info(f'Employee {employee_id} created a new reimbursement')
        conn.close()

    return reimbursement


def average_reimbursment():
    try:
        conn = get_connection()
        cur = conn.cursor()

        cur.execute(
            """SELECT round(avg(regexp_replace(riot_points::text, '[$,]', '', 'g')::numeric), 2) from reimbursements""")

        result = cur.fetchone()

    finally:
        dao_log.info(f'Average reimbursement cost is{result}')
        conn.close()

    return result


def most_money_spent():
    try:
        conn = get_connection()
        cur = conn.cursor()

        cur.execute("""select first_name, sum(riot_points) from reimbursements r
                    join employees e on r.author_id = e.employee_id
                    where status = 'approved'
                    group by first_name
                    Having SUM(riot_points) = (
                        SELECT MAX(mysum) FROM (
                            select first_name, sum(riot_points) mysum from reimbursements r
                            join employees e on r.author_id = e.employee_id
                            where status = 'approved'
                            group by first_name) AS x)""")

        result = cur.fetchone()

    finally:
        dao_log.info(f'{result} spends the most money')
        conn.close()

    return result


def most_request_made():
    try:
        conn = get_connection()
        cur = conn.cursor()

        cur.execute("""select e.first_name, count(r.reimbursement_id)from reimbursements r
                    join employees e on r.author_id = e.employee_id
                    group by e.first_name
                    HAVING count(r.reimbursement_id) = (
                        select MAX(mycount) FROM (
                            SELECT e.first_name, count(r.reimbursement_id) mycount from reimbursements r
                            join employees e on r.author_id = e.employee_id
                            group by e.first_name) AS x)""")

        result = cur.fetchone()
    finally:
        dao_log.info(f'{result} makes the most request')
        conn.close()

    return result
