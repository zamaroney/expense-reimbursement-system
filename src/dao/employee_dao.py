from src.sql.dbconfig import get_connection


def return_employee_info(username, password):
    try:
        conn = get_connection()
        cur = conn.cursor()
        cur.execute("SELECT * FROM employees WHERE emp_username=%s AND emp_password=%s", [username, password])
        employee = cur.fetchone()

        return employee
    finally:
        conn.close()
