# For sending our data in a json to postman
from json import JSONEncoder


class Employee:
    def __init__(self, employee_id, first_name, last_name, league_rank, email, username, password):
        self.employee_id = employee_id
        self.first_name = first_name
        self.last_name = last_name
        self.league_rank = league_rank
        self.email = email
        self.username = username
        self.password = password
