# For sending our data in a json to postman
from json import JSONEncoder


class Reimbursement:
    def __init__(self, reimbursement_id, author_id, riot_points, reason, status, created_time, manager_id, status_reason):
        self.reimbursement_id = reimbursement_id
        self.author_id = author_id
        self.riot_points = riot_points
        self.reason = reason
        self.status = status
        self.created_time = created_time
        self.manager_id = manager_id
        self.status_reason = status_reason


class ReimbursementEncoder(JSONEncoder):
    # override default method
    def default(self, reimbursement):
        if isinstance(reimbursement, Reimbursement):
            return reimbursement.__dict__
        else:
            return super().default(self, reimbursement)
