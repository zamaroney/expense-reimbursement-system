import src.dao.manager_dao as dao
from src.models.manager import Manager


def verify_manager_signin(username, password):
    if not dao.return_manager_info(username, password):
        return False
    else:
        return True


def return_manager_info(username, password):
    manager = dao.return_manager_info(username, password)
    manager_object = Manager(manager[0], manager[1], manager[2], manager[3], manager[4], manager[5], manager[6])

    return manager_object
