import src.dao.employee_dao as dao
from src.models.employee import Employee


def verify_employee_signin(username, password):
    if not dao.return_employee_info(username, password):
        return False
    else:
        return True


def return_employee_info(username, password):
    employee = dao.return_employee_info(username, password)
    employee_object = Employee(employee[0], employee[1], employee[2], employee[3], employee[4], employee[5], employee[6])

    return employee_object
