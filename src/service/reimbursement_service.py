import src.dao.reimbursement_dao as dao
from src.models.reimbursement import ReimbursementEncoder, Reimbursement
from json import dumps


def get_all_reimbursements():
    results = list()
    reimbursements = dao.get_all_reimbursements()
    for items in reimbursements:
        results.append(Reimbursement(items[0], items[1], items[2],
                                     items[3], items[4], str(items[5]), items[6], items[7]))
    return dumper(results)


def get_all_reimbursements_employee(employee_id):
    results = list()
    reimbursements = dao.get_all_reimbursements_employee(employee_id)
    for items in reimbursements:
        results.append(Reimbursement(items[0], items[1], items[2],
                                     items[3], items[4], str(items[5]), items[6], items[7]))
    return dumper(results)


def get_all_reimbursements_status(status):
    results = list()
    reimbursements = dao.get_all_reimbursements_employee(status)
    for items in reimbursements:
        results.append(Reimbursement(items[0], items[1], items[2],
                                     items[3], items[4], str(items[5]), items[6], items[7]))
    return dumper(results)


def verify_reimbursement_pending():
    if not dao.get_all_reimburseemnts_status('pending'):
        return False
    else:
        return True


def change_reimbursement_status(reimbursement_id, status, reason, manager_id):
    reimbursement = dao.change_reimbursement_status(reimbursement_id, status, reason, manager_id)
    reimbursement_object = Reimbursement(reimbursement[0], reimbursement[1], reimbursement[2], reimbursement[3],
                                         reimbursement[4], str(reimbursement[5]), reimbursement[6], reimbursement[7])
    return dumper(reimbursement_object)


def submit_reimbursements(employee_id, reason, money):
    reimbursement = dao.submit_reimbursements(employee_id, reason, money)
    reimbursement_object = Reimbursement(reimbursement[0], reimbursement[1], reimbursement[2], reimbursement[3],
                                         reimbursement[4], str(reimbursement[5]), reimbursement[6], reimbursement[7])
    return dumper(reimbursement_object)


def average_reimbursement():
    result = dao.average_reimbursment()[0]
    return dumps(str(result))


def most_money_spent():
    return dumps(dao.most_money_spent())


def most_request_made():
    return dumps(dao.most_request_made())


def dumper(dumpies):
    return dumps(dumpies, cls=ReimbursementEncoder)
