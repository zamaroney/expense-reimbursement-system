async function postFormDataAsJson({ url, formData }) {
	const plainFormData = Object.fromEntries(formData.entries());
	const formDataJsonString = JSON.stringify(plainFormData);

	const fetchOptions = {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
		body: formDataJsonString,
	};

	const response = await fetch(url, fetchOptions);

	if (!response.ok) {
		const errorMessage = await response.text();
		throw new Error(errorMessage);
	}

	return response.json();
}

function isEmpty(object){
    return Object.keys(object).length === 0;
}

async function handleFormSubmit(event) {
	event.preventDefault();

	const form = event.currentTarget;
	const url = form.action;

    const formData = new FormData(form);
    const responseData = await postFormDataAsJson({ url, formData });
    if(isEmpty(responseData)){
        document.getElementById('possibleError').innerText = "Invalid Credentials"
    }
    else{
		if('manager_id' in responseData){
			window.location.href = "http://127.0.0.1:5000/manager"
		}
		else{
			window.location.href = "http://127.0.0.1:5000/employee"
		}
    }
}


const exampleForm = document.getElementById("login_form");
exampleForm.addEventListener("submit", handleFormSubmit);