function getReimbursements(){
    let url = 'http://127.0.0.1:5000/reimbursements/employee'

    async function getapi(url) {
    
        // Storing response
        const response = await fetch(url);
        
        // Storing data in form of JSON
        let data = await response.json();
            console.log(data);
            show(data);
    }

    getapi(url);

    function show(data) {
        let tab = ``
        for (let r of data) {
            tab += `<tr> 
            <td>${r.reimbursement_id}</td>
            <td>${r.author_id}</td>
            <td>${r.reason}</td> 
            <td>${r.riot_points}</td>
            <td>${r.created_time}</td>
            <td>${r.status}</td>
            <td>${r.status_reason}</td>
            <td>${r.manager_id}</td>                     
            </tr>`;}
        // Setting innerHTML as tab variable
        document.getElementById("reimbursementListBody").innerHTML = tab;
    }
}

window.onload = function(){
    this.getReimbursements()
}


async function postFormDataAsJson({ url, formData }) {
	const plainFormData = Object.fromEntries(formData.entries());
	const formDataJsonString = JSON.stringify(plainFormData);

	const fetchOptions = {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
		body: formDataJsonString,
	};

	const response = await fetch(url, fetchOptions);

	if (!response.ok) {
		const errorMessage = await response.text();
		throw new Error(errorMessage);
	}

	return response.json();
}

function isEmpty(object){
    return Object.keys(object).length === 0;
}

async function handleFormSubmit(event) {
	event.preventDefault();

	const form = event.currentTarget;
	const url = form.action;

    const formData = new FormData(form);
    const responseData = await postFormDataAsJson({ url, formData });
    console.log(responseData);
    if(isEmpty(responseData)){
        document.getElementById('possibleError').innerText = "Invalid Credentials"
    }
    else{
        window.location.href = "http://127.0.0.1:5000/employee"
    }
}

const exampleForm = document.getElementById("login_form");
exampleForm.addEventListener("submit", handleFormSubmit);