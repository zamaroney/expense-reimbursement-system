function getAverage() {
    let url="http://127.0.0.1:5000/statistics/average"

    async function getapi(url) {
    
        // Storing response
        const response = await fetch(url);
        
        // Storing data in form of JSON
        let data = await response.json();
            console.log(data);
            document.getElementById("averageData").innerHTML = `$${data}`;
    }
    getapi(url);
}

function getMostSpent() {
    let url="http://127.0.0.1:5000/statistics/money"

    async function getapi(url) {
    
        // Storing response
        const response = await fetch(url);
        
        // Storing data in form of JSON
        let data = await response.json();
            console.log(data);
            document.getElementById("moneyData").innerHTML = `${data[0]} with ${data[1]} `;
    }
    getapi(url);
}
function getRequest() {
    let url="http://127.0.0.1:5000/statistics/reimbursement"

    async function getapi(url) {
    
        // Storing response
        const response = await fetch(url);
        
        // Storing data in form of JSON
        let data = await response.json();
            console.log(data);
            document.getElementById("requestData").innerHTML = `${data[0]} with ${data[1]} `;
    }
    getapi(url);
}



function getReimbursements(){
    let url = 'http://127.0.0.1:5000/reimbursements'

    async function getapi(url) {
    
        // Storing response
        const response = await fetch(url);
        
        // Storing data in form of JSON
        let data = await response.json();
            console.log(data);
            show(data);
    }

    getapi(url);

    function show(data) {
        let pendingTable = ``
        let approvedTable = ``
        let rejectedTable = ``
        for (let r of data) {
            if(r.status == 'pending'){
                pendingTable += `<tr> 
                <td>${r.reimbursement_id}</td>
                <td>${r.author_id}</td>
                <td>${r.reason}</td> 
                <td>${r.riot_points}</td>
                <td>${r.created_time}</td>
                <td>${r.status}</td>
                <td>${r.status_reason}</td>
                <td>${r.manager_id}</td>                     
                </tr>`;
            }
            else if(r.status == 'approved'){
                approvedTable += `<tr> 
                <td>${r.reimbursement_id}</td>
                <td>${r.author_id}</td>
                <td>${r.reason}</td> 
                <td>${r.riot_points}</td>
                <td>${r.created_time}</td>
                <td>${r.status}</td>
                <td>${r.status_reason}</td>
                <td>${r.manager_id}</td>                     
                </tr>`;
            }
            else if(r.status == 'rejected'){
                rejectedTable += `<tr> 
                <td>${r.reimbursement_id}</td>
                <td>${r.author_id}</td>
                <td>${r.reason}</td> 
                <td>${r.riot_points}</td>
                <td>${r.created_time}</td>
                <td>${r.status}</td>
                <td>${r.status_reason}</td>
                <td>${r.manager_id}</td>                     
                </tr>`;
            }
        }
        // Setting innerHTML as tab variable
        document.getElementById("pendingReimbursementBody").innerHTML = pendingTable;
        document.getElementById("approvedReimbursementBody").innerHTML = approvedTable;
        document.getElementById("rejectedReimbursementBody").innerHTML = rejectedTable;
    }
}

window.onload = function(){
    this.getReimbursements()
    this.getAverage()
    this.getMostSpent()
    this.getRequest()
}

async function postFormDataAsJson({ url, formData }) {
	const plainFormData = Object.fromEntries(formData.entries());
	const formDataJsonString = JSON.stringify(plainFormData);

	const fetchOptions = {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
		body: formDataJsonString,
	};

	const response = await fetch(url, fetchOptions);

	if (!response.ok) {
		const errorMessage = await response.text();
		throw new Error(errorMessage);
	}

	return response.json();
}

function isEmpty(object){
    return Object.keys(object).length === 0;
}

async function handleFormSubmit(event) {
	event.preventDefault();

	const form = event.currentTarget;
	const url = form.action;

    const formData = new FormData(form);
    const responseData = await postFormDataAsJson({ url, formData });
    console.log(responseData);
    if(isEmpty(responseData)){
        document.getElementById('possibleError').innerText = "Invalid Credentials"
    }
    else{
        console.log(responseData)
        window.location.href = "http://127.0.0.1:5000/manager"
    }
}

const exampleForm = document.getElementById("submitChange");
exampleForm.addEventListener("submit", handleFormSubmit);