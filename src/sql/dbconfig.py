import psycopg2
import os


def get_connection():
    return psycopg2.connect(
        host=os.environ['db_url'],
        port="5432",
        user=os.environ['db_postgres'],
        password=os.environ['db_password'],
        database="RiotReimbursementDB"
    )
