from src.app import app

from flask import request, Response, render_template, redirect, session
from src.logger import controller_log

import src.service.reimbursement_service as r_service
import src.service.employee_service as e_service
import src.service.manager_service as m_service


@app.route('/')
def default():
    controller_log.info('user brought to the login page')
    return redirect('/login')


@app.route('/employee', methods=['GET', 'POST'])
def employee():
    if request.method == 'POST':
        json = request.get_json()
        reason = json['reason']
        money = json['money']
        employee_id = session['id']
        if reason != '' and money != '':
            controller_log.info('new reimbursement created')
            return r_service.submit_reimbursements(employee_id, reason, money)
        else:
            controller_log.info('Incorrect reimbursement submitted')
            return {}
    if session.get('id') is None:
        return redirect("/login")
    return render_template("employee_page.html")


@app.route('/manager', methods=['GET', 'POST'])
def manager():
    if request.method == 'POST':
        json = request.get_json()
        reason = json['reason']
        reimbursement_id = json['reimbursementID']
        status = json['action-select']
        manager_id = session['id']
        if reason != '' and status != '' and reimbursement_id != '':
            controller_log.info(f'{reimbursement_id} changed to {status}')
            return r_service.change_reimbursement_status(reimbursement_id, status, reason, manager_id)
        else:
            controller_log.info('Attempted to change a reimbursement incorrectly')
            return {}
    return render_template("manager_page.html")


@app.route('/login', methods=['GET', 'POST'])
def login():
    controller_log.info('user at the login page')
    if request.method == 'POST':
        session['id'] = None
        session['name'] = None
        json = request.get_json()
        username = json['username']
        password = json['password']
        user_type = json['loginAs']

        if user_type == 'employee':
            if e_service.verify_employee_signin(username, password):
                received_employee = e_service.return_employee_info(username, password)
                session['id'] = received_employee.employee_id
                session['name'] = received_employee.first_name
                controller_log.info('Correct Employee credentials entered')
                return received_employee.__dict__
            else:
                controller_log.info('Incorrect Employee credentials entered')
                return {}
        if user_type == 'manager':
            if m_service.verify_manager_signin(username, password):
                received_manager = m_service.return_manager_info(username, password)
                session['id'] = received_manager.manager_id
                session['name'] = received_manager.first_name
                controller_log.info('Correct manager credentials entered')
                return received_manager.__dict__
            else:
                controller_log.info('Incorrect manager credentials entered')
                return {}

    return render_template("login_page.html")


@app.route('/reimbursements/employee')
def reimbursements_employee():
    results = r_service.get_all_reimbursements_employee(session['id'])
    return results


@app.route('/reimbursements')
def reimbursements():
    results = r_service.get_all_reimbursements()
    return results


@app.route('/statistics/average')
def average():
    return r_service.average_reimbursement()


@app.route('/statistics/money')
def money():
    return r_service.most_money_spent()


@app.route('/statistics/reimbursement')
def reimbursement():
    return r_service.most_request_made()
