import psycopg2
import unittest
import os
from unittest import mock
from src.sql import dbconfig


def mockconnection():
    test_conn = psycopg2.connect(
        host='localhost',
        port="5432",
        user=os.environ['db_postgres'],
        password=os.environ['db_password'],
        database="reimbursementTest"
    )
    dbconfig.get_connection = mock.Mock(return_value=test_conn)


class reimbursement_test(unittest.TestCase):

    def setUp(self):
        mockconnection()
        conn = dbconfig.get_connection(False)
        cur = conn.cursor()

        cur.execute("INSERT INTO reimbursements(author_id, riot_points, reason)"
                    "VALUES('1', '1300.00', 'need new skins')")
        conn.commit()

        cur.execute("INSERT INTO reimbursements(author_id, riot_points, reason, status)"
                    "VALUES('1', '1300.00', 'need new skins', 'approved')")
        conn.commit()

        cur.execute("INSERT INTO employees (first_name, last_name, league_rank, email, emp_username, emp_password)"
                    "VALUES ('Talon', 'Du Couteau', 'Bronze', 'bladeandsoul@gmail.com', 'edgey', 'assassinpath')")
        conn.commit()

    def test_get_all_reimbursements(self):
        mockconnection()
        conn = dbconfig.get_connection()
        cur = conn.cursor()

        cur.execute("SELECT * FROM reimbursements")

        query_rows = cur.fetchall()

        iterator = 1
        for row in query_rows:
            self.assertEqual(row[0], iterator)
            iterator = iterator + 1

    def test_get_reimbursements_employee(self):
        mockconnection()
        conn = dbconfig.get_connection()
        cur = conn.cursor()

        cur.execute("SELECT * FROM reimbursements Where author_id=1")

        query_rows = cur.fetchall()

        iterator = 1
        for row in query_rows:
            self.assertEqual(row[0], iterator)
            iterator = iterator + 1

    def test_get_reimbursements_status(self):
        mockconnection()
        conn = dbconfig.get_connection()
        cur = conn.cursor()

        cur.execute("SELECT * FROM reimbursements WHERE status='pending'")

        query_rows = cur.fetchall()

        iterator = 1
        for row in query_rows:
            self.assertEqual(row[0], iterator)
            iterator = iterator + 1

    def test_change_reimbursement_status(self):
        mockconnection()
        conn = dbconfig.get_connection()
        cur = conn.cursor()

        cur.execute("UPDATE reimbursements SET status='approved', status_reason='is good', manager_id='1'"
                    "where reimbursement_id='1' RETURNING *")
        reimbursement = cur.fetchone()
        conn.commit()

        self.assertEqual(reimbursement[6], 1)

    def test_submit_reimbursement(self):
        mockconnection()
        conn = dbconfig.get_connection()
        cur = conn.cursor()

        cur.execute("INSERT INTO reimbursements(author_id, riot_points, reason)"
                    "VALUES('1', '1300.00', 'need new skins') RETURNING *")
        reimbursement = cur.fetchone()
        conn.commit()

        self.assertEqual(reimbursement[0], 3)

    def test_average_reimbursements(self):
        mockconnection()
        conn = dbconfig.get_connection()
        cur = conn.cursor()

        cur.execute(
            "SELECT round(avg(regexp_replace(riot_points::text, '[$,]', '', 'g')::numeric), 2) from reimbursements")
        average = cur.fetchone()

        self.assertEqual(1300.00, average[0])

    def test_most_money_spent(self):
        mockconnection()
        conn = dbconfig.get_connection()
        cur = conn.cursor()

        cur.execute("""select first_name, sum(riot_points) from reimbursements r
                            join employees e on r.author_id = e.employee_id
                            where status = 'approved'
                            group by first_name
                            Having SUM(riot_points) = (
                                SELECT MAX(mysum) FROM (
                                    select first_name, sum(riot_points) mysum from reimbursements r
                                    join employees e on r.author_id = e.employee_id
                                    where status = 'approved'
                                    group by first_name) AS x)""")

        result = cur.fetchone()

        self.assertEqual('Talon', result[0])

    def test_most_request_made(self):
        mockconnection()
        conn = dbconfig.get_connection()
        cur = conn.cursor()

        cur.execute("""select e.first_name, count(r.reimbursement_id)from reimbursements r
                            join employees e on r.author_id = e.employee_id
                            group by e.first_name
                            HAVING count(r.reimbursement_id) = (
                                select MAX(mycount) FROM (
                                    SELECT e.first_name, count(r.reimbursement_id) mycount from reimbursements r
                                    join employees e on r.author_id = e.employee_id
                                    group by e.first_name) AS x)""")

        result = cur.fetchone()

        self.assertEqual('Talon', result[0])

    def tearDown(self):
        mockconnection()
        conn = dbconfig.get_connection()
        cur = conn.cursor()
        cur.execute("TRUNCATE reimbursements RESTART IDENTITY CASCADE")
        conn.commit()
