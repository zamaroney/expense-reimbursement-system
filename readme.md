# PROJECT NAME
Rito Reimbursement System
## Project Description

Rito Reimbursement System will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can log in and submit requests for reimbursements and view their past tickets and pending requests. Managers can log in and view all reimbursement requests and past history for all employees in the company. Managers are authorized to approve and deny requests for expense reimbursements.

## Technologies Used

* Python - version 3.9
* JavaScript - version ES6
* HTML
* CSS 3
* Postgres - version 13
* Flask - version 2.0.1
* AWS RDS
* SQL
* Pycharm
* Gitbash

## Features

List of features ready and TODOs for future development
* login feature for both employee and manager.
* Employees can submit reimbursement requests.
* Employees can view thier previously made reimbursement requests.
* Managers can approve or deny a reimbursement request.
* Managers can view all reimbursements made by employees.
* Managers can view some statistics about the reimbursements.

To-do list:
* Make managers able to submit a reimbursement to be reviewed by another manager, or a higher up.
* Add a logout button.

## Getting Started
   
To clone the repo, use git bash in a desired directory and run this command:
> $ git clone https://gitlab.com/zamaroney/expense-reimbursement-system.git

A list of all the packages used via pip install:
* behave          1.2.6
* bidict          0.21.2
* cachelib        0.2.0
* click           8.0.1
* colorama        0.4.4
* Flask           2.0.1
* Flask-Cors      3.0.10
* Flask-Session   0.4.0
* itsdangerous    2.0.1
* Jinja2          3.0.1
* MarkupSafe      2.0.1
* parse           1.19.0
* parse-type      0.5.2
* pip             21.1.2
* psycopg2        2.9.1
* python-engineio 4.2.0
* python-socketio 5.3.0
* selenium        3.141.0
* setuptools      3.3
* six             1.16.0
* urllib3         1.26.6
* Werkzeug        2.0.1

To install this go into the terminal in pycharm and use the command:
> $ pip install (package name)

To get the database up and running, be sure to have postgres 10 or higher installed on your computer,  
get that here: https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

As well as PGadmin: https://www.pgadmin.org/download/

After running this set up a databse at any location of your choice, this project is based on an AMAZON RDS.

Run these commands in the PGadmin query tool to set up the database tables:  
For employees table:  
![Employee Table](/images/employee_table.png)  
For managers table:  
![Manager Table](/images/manager_table.png)  
For reimbursements table:  
![Reimbursements Table](/images/reimbursement_table.png)  
  
You will also need to insert employees and managers into the tables.
  
After these steps you are ready to run the web application.

## Usage

To open up the server run the main.py file in pycharm and let it stay open.  

After you run it you will be prompted this message in the pycharm console:  
![Website Link](/images/click_for_website.png)  
Click on the link.  

You will be brought to the login page where you can sign in:  
![login Page](/images/Login_page.png)  
  
for an employee you can submit a reimbursment request:  
![Emplotee Submit View](/images/Employee_submit.png)  
  
You can also click on the reimbursement list tab to view previous reimbursement and their statuses:  
![Emplotee Reimbursement List View](/images/employee_list.png)  
  
As a manager the approve or deny pending reimbursements:  
![Manager Submit View](/images/manager_submit.png)  
  
You can also click on the approved tab to view aprroved reimbursements:  
![Approved Tab](/images/manager_approved.png)  
  
You can click on the rejected tab to view rejected reimbursements:  
![Rejected Tabl](/images/manager_rejected.png)  

Finally you can click the statistics tab to see some statistics:  
![Statistics Tab](/images/manager_stats.png)  
  
## License

This project uses the following license: [license](/LICENSE).
